# Ising Model in Julia

This project simulates the [Ising model](https://en.wikipedia.org/wiki/Ising_model) using the Kawasaki dynamic (i.e. only opposing neighboring spins are exchanged), or the Metropolis-Hastings dynamic (i.e. any spin may be flipped).

See [gitlab.com/vogier/Beamer](https://gitlab.com/vogier/Beamer) for an explanation, and for sample outputs.
